#include "cudaFunctions.h"
#include <stdio.h>
#define BLOCK_SIZE 16

// teste
// Multiplica matrizes NxN usando Cuda
// A grid de blocos sera a matrix resultante
// uma thread tem a responsabilidade de fazer toda a multiplicacao
// maX = matrix A colunas
// maY = matrix A linhas
__global__ void multiplyMatrixCuda(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY)
{
    // int index = threadIdx.x + (blockDim.x * blockIdx.x);

    // onde comeca cada um dos vetores
    long indexLinha = (blockIdx.y * maY);
    long indexColuna = blockIdx.x;
    long indexResultado = blockIdx.x + (blockIdx.y * mbY);

    // deslocamento do index por causa da thread
    long indexThared = threadIdx.x;

    for (long index = indexThared; index < mbX; index += blockDim.x)
    {
        c[indexResultado] += a[indexLinha + index] * b[(index * mbY) + indexColuna];
    }
}

// Esse kernel só vai precisa multiplicar os 2 vetores
// o vetor A vou utilizar ele de forma linear
// o vetor B nao esta alocando linearmente pra multiplicao que preciso fazer
// so precisa de um bloco executar
__global__ void multiplyMatrixCudaStream(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY, int streamX, int streamY)
{
    // int index = threadIdx.x + (blockDim.x * blockIdx.x);

    // int indexLinha = (blockIdx.y * maY);
    // int indexColuna = blockIdx.x;
    // int indexResultado = blockIdx.x + (blockIdx.y * mbY);

    // onde comeca cada um dos vetores
    unsigned long indexColuna = blockIdx.x;
    unsigned long indexResultado = blockIdx.x + (streamY * mbY);

    // deslocamento do index por causa da thread
    unsigned long indexThared = threadIdx.x;

    __shared__ int arrSum[256];
    __shared__ int sum;
    int soma = 0;
    sum = 0;

    int resultado = 0;
    for (unsigned long index = indexThared; index < mbX; index += blockDim.x)
    {
        resultado += a[index] * b[(index * mbY) + indexColuna];
    }

    arrSum[threadIdx.x] = resultado;

    __syncthreads();
    if (threadIdx.x == 0)
    {
        for (int i = 0; i < blockDim.x; i++)
        {
            sum += arrSum[i];
        }

        c[indexResultado] = sum;
    }
}

// Esse kernel só vai precisa multiplicar os 2 vetores
// o vetor A vou utilizar ele de forma linear
// o vetor B nao esta alocando linearmente pra multiplicao que preciso fazer
// so precisa de um bloco executar
__global__ void multiplyMatrixCudaStream2(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY, int streamX, int streamY)
{
    // int index = threadIdx.x + (blockDim.x * blockIdx.x);

    // int indexLinha = (blockIdx.y * maY);
    // int indexColuna = blockIdx.x;
    // int indexResultado = blockIdx.x + (blockIdx.y * mbY);

    // onde comeca cada um dos vetores
    unsigned long indexColuna = blockIdx.x;
    unsigned long indexResultado = blockIdx.x + (streamY * mbY);

    // deslocamento do index por causa da thread
    unsigned long indexThared = threadIdx.x;

    __shared__ int arrSum[256];
    extern __shared__ int arrShared[];
    __shared__ int sum;
    int soma = 0;
    sum = 0;

    for (unsigned long index = indexThared; index < mbX; index += blockDim.x)
    {
        arrShared[index] = a[index];
    }

    int resultado = 0;
    for (unsigned long index = indexThared; index < mbX; index += blockDim.x)
    {
        resultado += arrShared[index] * b[(index * mbY) + indexColuna];
    }

    arrSum[threadIdx.x] = resultado;

    __syncthreads();
    if (threadIdx.x == 0)
    {
        for (int i = 0; i < blockDim.x; i++)
        {
            sum += arrSum[i];
        }

        c[indexResultado] = sum;
    }
}

__global__ void sumCuda(int *a, int *b, int *c, int tamanho)
{

    int index = threadIdx.x + (blockDim.x * blockIdx.x);
    if (index < tamanho)
    {
        c[index] = a[index] + b[index];
    }
}

__global__ void sumMatrixCuda(int *a, int *b, int *c, int tamanho)
{

    int index = threadIdx.x + (blockDim.x * blockIdx.x);
    if (index < tamanho)
    {
        c[index] = a[index] + b[index];
    }
}

// copiado apra comparacao https://github.com/lzhengchun/matrix-cuda/blob/master/mm_omp_vs_cuda.cu
__global__ void gpu_matrix_mult(int *a, int *b, int *c, int m, int n, int k)
{
    // de termina a linha e acoluna que vai ser acessada nas matrizes de entrada a partir dos blocos
    // cada thread eh responsavel poor multiplicar os vetores
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    int sum = 0;
    if (col < k && row < m)
    {
        for (int i = 0; i < n; i++)
        {
            sum += a[row * n + i] * b[i * k + col];
        }
        // o resultado eh colocado na memoria global
        c[row * k + col] = sum;
    }
}

// vou conciderar apenas a stream do vetor a
__global__ void gpu_matrix_mult_stream(int *a, int *b, int *c, int m, int n, int k, int streamIndex)
{
    // de termina a linha e acoluna que vai ser acessada nas matrizes de entrada a partir dos blocos
    // cada thread eh responsavel poor multiplicar os vetores
    int row = streamIndex * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    int sum = 0;
    if (col < k && row < m)
    {
        for (int i = 0; i < n; i++)
        {
            sum += a[i] * b[i * k + col];
        }
        // o resultado eh colocado na memoria global
        c[row * k + col] = sum;
    }
}

bool testarSuporteStream()
{
    cudaDeviceProp properties;
    int deviceHandler;

    cudaGetDevice(&deviceHandler);
    cudaGetDeviceProperties(&properties, deviceHandler);

    // enteder os retornos
    if (properties.asyncEngineCount <= 1)
    {
        return false;
    }
    return true;
}

/*

  */

// Multiplica matrizes NxN usando Cuda
// A grid de blocos sera a matrix resultante
// uma thread tem a responsabilidade de fazer toda a multiplicacao
// maX = matrix A colunas
// maY = matrix A linhas
/*
__global__ void multiplyMatrixCuda(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY)
{
    // int index = threadIdx.x + (blockDim.x * blockIdx.x);

    // onde comeca cada um dos vetores
    int indexLinha = (blockIdx.y * maY);
    int indexColuna = blockIdx.x;

    int indexResultado = blockIdx.x + (blockIdx.y * mbY);
    for (int index = 0; index < mbX; index++)
    {
        c[indexResultado] += a[indexLinha + index] * b[(index * mbY) + indexColuna];
    }
}*/