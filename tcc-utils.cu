#include "tcc-utils.h"
#include <iostream>
#include <stdio.h>
#include "cuda_runtime.h"
// testea

void somarVetores(int *a, int *b, int *resultado, int tamanho)
{
    for (int index = 0; index < tamanho; index++)
    {
        resultado[index] = a[index] + b[index];
    }
}

void somarMatrizes(int *a, int *b, int *resultado, int tamanho)
{
    for (int index = 0; index < tamanho; index++)
    {
        resultado[index] = a[index] + b[index];
    }
}

// maX = matrix A colunas
// maY = matrix A linhas
void multiplicarMatrizes(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{
    if (maY != mbX)
    {
        cout << " Não pode multiplicar" << endl;
        return;
    }

    long tamanhoResultadoX = maX;
    long tamanhoResultadoY = mbY;

    cudaEvent_t start, stop;

    cudaEventCreate(&start);

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    for (long indexY = 0; indexY < tamanhoResultadoY; indexY++)
    {
        for (long indexX = 0; indexX < tamanhoResultadoX; indexX++)
        {
            long indexResultado = indexX + (indexY * tamanhoResultadoY);
            resultado[indexResultado] = 0;
            for (long index = 0; index < maY; index++)
            {
                long indexLinha = index + (indexY * maY);
                long indexColuna = indexX + (index * mbY);
                resultado[indexResultado] += a[indexLinha] * b[indexColuna];
            }
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total CPU: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

void validarResultado(int *out, int *out_teste, unsigned long tamanho)
{
    bool flag = true;
    for (unsigned long index = 0; index < tamanho; index++)
    {
        if (out[index] != out_teste[index])
        {
            flag = false;
            cout << "Erro index: " << index << " cuda: " << out[index] << " cpu: " << out_teste[index] << endl;
        }
    }
    if (!flag)
    {
        cout << "Erro de multiplicação das matrizes." << endl;
    }
    else
    {
        cout << "Sucesso na multiplicação das matrizes" << endl;
    }
}

void printarMatriz(int *matriz, int tamanhoX, int tamanhoY)
{
    cout << endl;
    for (int indexY = 0; indexY < tamanhoY; indexY++)
    {
        string linha = "";
        for (int indexX = 0; indexX < tamanhoX; indexX++)
        {
            int indexResultado = indexX + (indexY * tamanhoX);
            linha += " " + to_string(matriz[indexResultado]) + " ";
        }
        cout << linha << endl;
    }
}

void printarVetor(int *matriz, int tamanhoX)
{
    cout << endl;
    string linha = "";
    for (int indexX = 0; indexX < tamanhoX; indexX++)
    {
        linha += " " + to_string(matriz[indexX]) + " ";
    }
    cout << linha << endl;
}

int random(int min, int max) // range : [min, max]
{
    static bool first = true;
    if (first)
    {
        srand(time(NULL)); // seeding for the first time only!
        first = false;
    }
    return min + rand() % ((max + 1) - min);
}

CudaMatrix *createMatrixFloat(int rows, int columns)
{
    CudaMatrix *matrix = (CudaMatrix *)malloc(sizeof(CudaMatrix));
    // cudaHostAlloc((void **)&matrix, sizeof(float) * rows * columns, cudaHostAllocDefault);
    matrix->rows = rows;
    matrix->columns = columns;
    cudaHostAlloc((void **)&matrix->matrix, sizeof(float) * rows * columns, cudaHostAllocDefault);

    return matrix;
}

void printCublasMatrix(CudaMatrix *matrix, string matrixName)
{
    cout << matrixName << endl;
    for (int a = 0; a < matrix->rows; a++)
    {
        for (int b = 0; b < matrix->columns; b++)
        {
            cout << matrix->matrix[toPositionMatrixCublas(a, b, matrix->columns)] << " ";
        }
        cout << endl;
    }
    cout << endl;
}
// considere a matrix guarda em column-wise
int toPositionMatrixCublas(int row, int column, int sizeColumn)
{
    return (sizeColumn * column) + row;
}

void fillCublasMatrix(CudaMatrix *matrix, int type)
{
    float cont = 1;
    for (int a = 0; a < matrix->rows; a++)
    {
        for (int b = 0; b < matrix->columns; b++)
        {

            float value = 0;
            switch (type)
            {
            case 1:
                value = random(0, 10);
                break;
            case 2:
                value = cont;
                cont += 1;
                break;

            default:
                value = 0;
                break;
            }
            matrix->matrix[toPositionMatrixCublas(a, b, matrix->columns)] = value;
        }
    }
}

bool validarResultadoCublasMatrix(CudaMatrix *matrixA, CudaMatrix *matrixB)
{

    bool flag = true;
    for (int a = 0; a < matrixA->rows; a++)
    {
        for (int b = 0; b < matrixB->columns; b++)
        {
            if (matrixA->matrix[a * matrixA->rows + b] != matrixB->matrix[a * matrixA->rows + b])
            {
                cout << "Erro matrix posicao " << a << " " << b << endl;
            }
        }
    }
    if (flag)
    {
        cout << "Matrizes corretas" << endl;
    }
    else
    {
        cout << "matrizes incorretas" << endl;
    }
    return flag;
}

void freeMatrixFloat(CudaMatrix *matrix)
{
    free(matrix->matrix);
    free(matrix);
}