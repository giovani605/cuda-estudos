output: main.o tcc-utils.o cudaFunctions.o cudaTests.o cublasFunctions.o
	nvcc tcc-utils.o cudaFunctions.o main.o cudaTests.o  cublasFunctions.o -g -G  -o output -allow-unsupported-compiler -lcublas
cudaFunctions.o: cudaFunctions.cu
	nvcc -g -G  -c  cudaFunctions.cu -allow-unsupported-compiler
tcc-utils.o: tcc-utils.cu
	nvcc -g -G  -c tcc-utils.cu  -allow-unsupported-compiler
cudaTests.o: cudaTests.cu
	nvcc -g -G  -c cudaTests.cu -allow-unsupported-compiler
cublasFunctions.o: cublasFunctions.cu
	nvcc -g -G  -c cublasFunctions.cu -allow-unsupported-compiler
main.o: main.cu
	nvcc -g -G  -c main.cu  -allow-unsupported-compiler
clean:
	rm *.o output