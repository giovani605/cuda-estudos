// esse arquivo vai ser para fazer as chamadas de Cuda de forma encaplusulada (como que escreve)
#include "cudaFunctions.h"

#include "cudaFunctions.h"

// maX = matrix A colunas
// maY = matrix A linhas
void cudaCallMatrixMul(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);

// maX = matrix A colunas
// maY = matrix A linhas
void cudaCallMatrixMulStream(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);

// maX = matrix A colunas
// maY = matrix A linhas
// Metodo encontrado na internet
void cudaCallMatrixMulDefault(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);

void cudaCallMatrixMulDefaultStream(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);
