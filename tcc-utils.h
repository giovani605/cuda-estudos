#pragma once
#include <string>

// essa matrix sera guarda na memoria a partir da colunas (column-wise)
typedef struct CudaMatrix
{
    float *matrix;
    int rows;
    int columns;
} CudaMatrix;
using namespace std;

void somarVetores(int *a, int *b, int *resultado, int tamanho);

void somarMatrizes(int *a, int *b, int *resultado, int tamanho);

// maX = matrix A colunas
// maY = matrix A linhas
void multiplicarMatrizes(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);

void validarResultado(int *out, int *out_teste, unsigned long tamanho);

void printarMatriz(int *matriz, int tamanhoX, int tamanhoY);

void printarVetor(int *matriz, int tamanhoX);

int random(int min, int max);

CudaMatrix *createMatrixFloat(int rows, int columns);

void freeMatrixFloat(CudaMatrix *matrix);

void printCublasMatrix(CudaMatrix *matrix, string matrixName);

void fillCublasMatrix(CudaMatrix *matrix, int type);

bool validarResultadoCublasMatrix(CudaMatrix *matrixA, CudaMatrix *matrixB);

int toPositionMatrixCublas(int row, int column, int sizeColumn);