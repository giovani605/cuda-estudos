#include "tcc-utils.h"

void testeCublas();

void cudaMatrixMultiply(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY);

void cublasMatrixMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado);

void cublasMatrixVectorMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado);

void cpuMatrixMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado);

void matrixCublasPrintVector(CudaMatrix *matrix);
