#include "cuda_device_runtime_api.h"
#include "cuda_runtime.h"
#include "cuda.h"
#include "cublas_v2.h"
#include "cuda.h"
#include <iostream>
#include "tcc-utils.h"
#define IDX2F(i, j, ld) ((((j)-1) * (ld)) + ((i)-1))

using namespace std;

void converterMatrizParaCublas(int *matrix, float *matrixCublas, int x, int y)
{
    for (int a = 0; a < x; a++)
    {
        for (int b = 0; b < y; b++)
        {
            // row-based
            int indexResultadoRowBased = a + (b * x);
            // Column-based
            int indexResultadoColumnBased = b + (a * y);

            matrixCublas[indexResultadoColumnBased] = matrix[indexResultadoRowBased];
        }
    }
}

void converterMatrizParaResultado(int *matrix, float *matrixCublas, int x, int y)
{
    for (int a = 0; a < x; a++)
    {
        for (int b = 0; b < y; b++)
        {
            // row-based
            int indexResultadoRowBased = a + (b * x);
            // Column-based
            int indexResultadoColumnBased = b + (a * y);

            matrix[indexResultadoRowBased] = matrixCublas[indexResultadoColumnBased];
        }
    }
}

void testeCublas()
{
    cout << "Cublas teste";
}

void printMatrizFloat(float *matrixCublas, int x, int y)
{
    for (int a = 0; a < x; a++)
    {
        for (int b = 0; b < y; b++)
        {
            int indexResultadoColumnBased = b + (a * y);

            cout << matrixCublas[indexResultadoColumnBased] << " ";
        }
        cout << endl;
    }
}

void cudaMatrixMultiply(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{

    cublasStatus_t stat;

    cudaEvent_t start, stop;

    cudaEventCreate(&start);

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    // alocar para o cublas
    float *cudaMatrixA;
    float *cudaMatrixB;
    float *resultadoCublas;
    long tamanhoTotal = maX * maY;
    long size = sizeof(int) * (tamanhoTotal);

    cudaHostAlloc((void **)&cudaMatrixA, size, cudaHostAllocDefault);

    cudaHostAlloc((void **)&cudaMatrixB, size, cudaHostAllocDefault);

    cudaHostAlloc((void **)&resultadoCublas, size, cudaHostAllocDefault);

    converterMatrizParaCublas(a, cudaMatrixA, maX, maY);
    converterMatrizParaCublas(b, cudaMatrixB, mbX, mbY);

    // printMatrizFloat(cudaMatrixA, maX, maY);

    // cout << endl
    //      << "-------------------" << endl;
    //  printarMatriz(a, maX, maY);
    // cout << endl
    //      << "-------------------" << endl;

    // calcular
    cublasHandle_t handle;
    cublasCreate(&handle);

    const float alf = 1;
    const float bet = 1;
    const float *alpha = &alf;
    const float *beta = &bet;

    // entender esses parametros
    int lda = maX, ldb = mbY, ldc = mbX;

    // Do the actual multiplication
    stat = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, maY, mbX, maX,
                       alpha, cudaMatrixA, lda, cudaMatrixB, ldb,
                       beta, resultadoCublas, ldc);
    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        printf("error cublas cublasSgemm");
    }
    cout << "print resultado cublas" << endl;
    printMatrizFloat(resultadoCublas, maX, maY);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo multiplicação CUBLAS: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // Destroy the handle
    cublasDestroy(handle);

    converterMatrizParaResultado(resultado, resultadoCublas, mbX, mbY);

    // cout << "print resultado cublas corrigido" << endl;
    // printarMatriz(resultado, mbX, mbY);
    //  ajustar para a volta o resultado
}

void cublasMatrixVectorMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado)
{

    cudaEvent_t start, stop;

    cudaEventCreate(&start);

    // calcular
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasStatus_t stat;
    const float alf = 1;
    const float bet = 1;
    const float *alpha = &alf;
    const float *beta = &bet;

    // entender esses parametros
    int lda = matrixA->rows, ldb = matrixB->columns, ldc = matrixA->rows;

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    float *cudaMatrixA = matrixA->matrix;
    float *cudaMatrixB = matrixB->matrix;
    float *resultadoCublas = resultado->matrix;
    //
    // cout << cudaMatrixA << " " << cudaMatrixB << " " << resultadoCublas << " " << beta << " " << alpha << endl;

    int kl = 0;
    int ku = 0;
    int incx = 1;
    int incy = 1;

    stat = cublasSgemv(handle, CUBLAS_OP_N, matrixA->rows, matrixA->columns,
                       alpha, cudaMatrixA, lda, cudaMatrixB, incx,
                       beta, resultadoCublas, incy);

    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        printf("error cublas cublasSgbmv");
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cublas matrix-vector: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}
void cublasMatrixMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado)
{

    cudaEvent_t start, stop;

    cudaEventCreate(&start);

    // calcular
    cublasHandle_t handle;
    cublasCreate(&handle);
    cublasStatus_t stat;
    const float alf = 1;
    const float bet = 1;
    const float *alpha = &alf;
    const float *beta = &bet;

    // entender esses parametros
    int lda = matrixA->rows, ldb = matrixB->columns, ldc = matrixA->rows;

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    float *cudaMatrixA = matrixA->matrix;
    float *cudaMatrixB = matrixB->matrix;
    float *resultadoCublas = resultado->matrix;
    //
    cout << cudaMatrixA << " " << cudaMatrixB << " " << resultadoCublas << " " << beta << " " << alpha << endl;

    stat = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, matrixA->columns, matrixB->rows, matrixA->rows,
                       alpha, cudaMatrixA, lda, cudaMatrixB, ldb,
                       beta, resultadoCublas, ldc);

    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        printf("error cublas cublasSgemm");
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cublas: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

void matrixCublasPrintVector(CudaMatrix *matrix)
{

    for (long index = 0; index < matrix->rows * matrix->columns; index++)
    {
        cout << matrix->matrix[index] << " ";
    }
    cout << endl;
}

void cpuMatrixMulti(CudaMatrix *matrixA, CudaMatrix *matrixB, CudaMatrix *resultado)
{

    cudaEvent_t start, stop;

    cudaEventCreate(&start);

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);
    matrixCublasPrintVector(matrixA);

    for (long indexY = 0; indexY < resultado->columns; indexY++)
    {
        for (long indexX = 0; indexX < resultado->rows; indexX++)
        {
            long indexResultado = toPositionMatrixCublas(indexX, indexY, resultado->columns);
            resultado->matrix[indexResultado] = 0;
            //  cout << "index Resultado" << indexResultado << endl;
            for (long index = 0; index < resultado->rows; index++)
            {
                // quero a linha
                long indexMatrixA = toPositionMatrixCublas(indexX, index, matrixA->columns);
                // quero a coluna
                long indexMatrixB = toPositionMatrixCublas(index, indexY, matrixB->columns);
                //    cout << "index multi" << index << " multiply " << matrixA->matrix[indexMatrixA] << "x" << matrixB->matrix[indexMatrixB] << endl;

                resultado->matrix[indexResultado] += matrixA->matrix[indexMatrixA] * matrixB->matrix[indexMatrixB];
            }
        }
    }

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total CPU: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}