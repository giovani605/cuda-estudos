#include <iostream>
#include <stdio.h>
#include <string.h>
#include "cudaFunctions.h"
#include "cuda_device_runtime_api.h"
#include "cuda.h"
#include "cublas_v2.h"
#include "tcc-utils.h"
#include "cudaTests.h"
#include "cublasFunctions.h"

#define N 3
#define M 3

using namespace std;

void calcularCublas()
{
    long colunas = N;
    long linhas = M;
    long tamanhoTotal = colunas * linhas;

    long size = sizeof(int) * (tamanhoTotal);
    long sizeResultado = sizeof(int) * (linhas * linhas);

    // declarar minhas matrizes
    CudaMatrix *matrixA = createMatrixFloat(linhas, colunas);
    CudaMatrix *matrixB = createMatrixFloat(linhas, colunas);
    CudaMatrix *matrixResultadoCpu = createMatrixFloat(linhas, colunas);
    CudaMatrix *matrixResultadoGpu = createMatrixFloat(linhas, colunas);

    // preencher as matrizes

    fillCublasMatrix(matrixA, 2);
    fillCublasMatrix(matrixB, 2);
    fillCublasMatrix(matrixResultadoCpu, 0);
    fillCublasMatrix(matrixResultadoGpu, 0);

    // printar
    printCublasMatrix(matrixA, "Matrix A");
    printCublasMatrix(matrixB, "Matrix B");
    printCublasMatrix(matrixResultadoCpu, "Matrix Resultado cpu");
    printCublasMatrix(matrixResultadoGpu, "Matrix Resultado gpu");

    // calcular pela cpu
    cpuMatrixMulti(matrixA, matrixB, matrixResultadoCpu);
    printCublasMatrix(matrixResultadoCpu, "Matrix Resultado cpu");

    // calcular pela gpu

    cublasMatrixMulti(matrixA, matrixB, matrixResultadoGpu);
    printCublasMatrix(matrixResultadoGpu, "Matrix Resultado Gpu");

    // testar resultado

    validarResultadoCublasMatrix(matrixResultadoGpu, matrixResultadoCpu);
}

void calcularCublasVector()
{
    long colunas = N;
    long linhas = M;
    long tamanhoTotal = colunas * linhas;

    long size = sizeof(int) * (tamanhoTotal);
    long sizeResultado = sizeof(int) * (linhas * linhas);

    // declarar minhas matrizes
    CudaMatrix *matrixA = createMatrixFloat(linhas, colunas);
    CudaMatrix *matrixB = createMatrixFloat(linhas, 1);
    CudaMatrix *matrixResultadoCpu = createMatrixFloat(linhas, 1);
    CudaMatrix *matrixResultadoGpu = createMatrixFloat(linhas, 1);

    // preencher as matrizes

    fillCublasMatrix(matrixA, 2);
    fillCublasMatrix(matrixB, 2);
    fillCublasMatrix(matrixResultadoCpu, 0);
    fillCublasMatrix(matrixResultadoGpu, 0);
    // printar
    printCublasMatrix(matrixA, "Matrix A");
    printCublasMatrix(matrixB, "Matrix B");
    printCublasMatrix(matrixResultadoCpu, "Matrix Resultado cpu");
    printCublasMatrix(matrixResultadoGpu, "Matrix Resultado gpu");

    // calcular pela cpu
    cpuMatrixMulti(matrixA, matrixB, matrixResultadoCpu);
    printCublasMatrix(matrixResultadoCpu, "Matrix Resultado cpu");

    // calcular pela gpu

    cublasMatrixVectorMulti(matrixA, matrixB, matrixResultadoGpu);
    printCublasMatrix(matrixResultadoGpu, "Matrix Resultado Gpu");

    // testar resultado

    validarResultadoCublasMatrix(matrixResultadoGpu, matrixResultadoCpu);
}

int main()
{
    cudaDeviceReset();
    cudaError_t cudaStat;
    cublasStatus_t stat;
    cublasHandle_t handle;

    if (!testarSuporteStream())
    {
        cout << "ESSA GPU NAO TEM SUPORTE." << endl;
        return 0;
    }

    stat = cublasCreate(&handle);
    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        printf("CUBLAS initialization failed\n");
        return EXIT_FAILURE;
    }

    cout << " Teste multiplicação de matrix tamanho: " << M << "x" << N << endl;
    calcularCublasVector();

    cout << " Teste multiplicação de matrix tamanho: " << M << "x" << N << endl;
    calcularCublas();

    int *a, *b, *out, *out_teste;

    long colunas = N;
    long linhas = M;
    long tamanhoTotal = colunas * linhas;

    long size = sizeof(int) * (tamanhoTotal);
    long sizeResultado = sizeof(int) * (linhas * linhas);

    // Allocate memory
    cudaHostAlloc((void **)&a, size, cudaHostAllocDefault);

    cudaHostAlloc((void **)&b, size, cudaHostAllocDefault);

    cudaHostAlloc((void **)&out, sizeResultado, cudaHostAllocDefault);

    cudaHostAlloc((void **)&out_teste, sizeResultado, cudaHostAllocDefault);

    for (long index = 0; index < tamanhoTotal; index++)
    {
        a[index] = random(0, 10);
        b[index] = random(0, 10);
        // a[index] = index + 1;
        // b[index] = index + 1;

        // a[index] = 1;
        // b[index] = 1;
    }

    // cout << "print matrizes " << endl;
    // printarMatriz(a, colunas, linhas);
    // cout << "print matrizes " << endl;
    // printarMatriz(b, linhas, colunas);

    // multiplicarMatrizes(a, b, out_teste, linhas, colunas, colunas, linhas);
    // cudaMatrixMultiply(a, b, out, linhas, colunas, colunas, linhas);
    // cudaCallMatrixMul(a, b, out, linhas, colunas, colunas, linhas);
    // cudaCallMatrixMulDefaultStream(a, b, out, linhas, colunas, colunas, linhas);
    // cudaCallMatrixMulDefault(a, b, out, linhas, colunas, colunas, linhas);
    // cudaCallMatrixMulStream(a, b, out, linhas, colunas, colunas, linhas);
    // cout << "RESULTADO MULTIPLICACAO" << endl;
    // printarMatriz(out_teste, linhas, linhas);
    //  printarVetor(out_teste, linhas * linhas);

    // cout << "RESULTADO MULTIPLICACAO CUDA" << endl;
    // printarMatriz(out, linhas, linhas);

    // validarResultado(out, out_teste, (unsigned long)linhas * linhas);

    cudaFreeHost(a);
    cudaFreeHost(b);
    cudaFreeHost(out);
    cudaFreeHost(out_teste);
}
