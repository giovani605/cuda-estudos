// esse arquivo vai ser para fazer as chamadas de Cuda de forma encaplusulada (como que escreve)
#include "cudaTests.h"

#include <iostream>

using namespace std;

// Essa nomenclatura ta ruim
// maX = matrix A colunas
// maY = matrix A linhas
void cudaCallMatrixMul(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{

    cudaEvent_t start, middle, stop;

    cudaEventCreate(&start);
    cudaEventCreate(&middle);

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    int linhas = maX;
    int colunas = maY;

    int tamanhoTotal = colunas * linhas;

    int size = sizeof(int) * (tamanhoTotal);
    int sizeResultado = sizeof(int) * (linhas * linhas);

    int *a_dev;
    int *b_dev;
    int *out_dev;

    cudaMalloc((void **)&a_dev, size);
    cudaMalloc((void **)&b_dev, size);
    cudaMalloc((void **)&out_dev, sizeResultado);

    cudaMemcpy(a_dev, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(b_dev, b, size, cudaMemcpyHostToDevice);

    int thread = 256;

    dim3 threadsPerBlock(linhas, linhas);

    cudaEventRecord(middle, 0);
    cudaEventSynchronize(middle);

    float elapsedMemoryTime;
    cudaEventElapsedTime(&elapsedMemoryTime, start, middle);
    printf("Tempo tansferencia memoria Cuda sem stream: %3.1f ms \n", elapsedMemoryTime);

    multiplyMatrixCuda<<<threadsPerBlock, thread>>>(a_dev, b_dev, out_dev, linhas, colunas, colunas, linhas);
    cudaDeviceSynchronize();

    cudaMemcpy(resultado, out_dev, sizeResultado, cudaMemcpyDeviceToHost);

    cudaFree(a_dev);
    cudaFree(b_dev);
    cudaFree(out_dev);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cuda sem stream: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaEventDestroy(middle);
}

// Essa nomenclatura ta ruim
// maX = matrix A colunas
// maY = matrix A linhas
// apenas strema vetor A
// essa funcao copia primeiro a matrix B inteira e dai strame a matrix A
// cada stream resolve uma linha na matrix de resultado
void cudaCallMatrixMulStream(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{

    long linhas = maX;
    long colunas = maY;

    long tamanhoTotal = colunas * linhas;

    long size = sizeof(int) * (tamanhoTotal);
    long sizeResultado = sizeof(int) * (linhas * linhas);

    // criar streams
    cudaStream_t *streams;
    // cria uma stream para cada linha da matriz de resultado
    long qtdStreams = linhas;

    streams = (cudaStream_t *)malloc(sizeof(cudaStream_t) * qtdStreams);

    bool flag = true;
    // alocar uam stream para cada resultado naov ai dar
    // e talbez nao faça muito sentido
    // estou criando um overhead absurdo
    for (long index = 0; index < qtdStreams; index++)
    {
        cudaError_t error = cudaStreamCreate(&streams[index]);
        if (error > 0)
        {
            flag = false;
            cout << "Não foi possivel alocar as streams " << cudaGetErrorName(error) << " STREAM ID: " << index << endl;
            return;
        }
    }

    cudaEvent_t start, middle, stop;

    cudaEventCreate(&start);

    cudaEventCreate(&stop);
    cudaEventRecord(start, streams[0]);
    cudaEventCreate(&middle);

    int *a_dev;
    int *b_dev;
    int *out_dev;

    cudaMalloc((void **)&a_dev, size);
    cudaMalloc((void **)&b_dev, size);
    cudaMalloc((void **)&out_dev, sizeResultado);

    int thread = 32;

    // nao precise de blocos
    // dim3 threadsPerBlock(linhas, linhas);

    unsigned long sizeLinha = sizeof(int) * colunas;

    unsigned long sizeResposta = sizeof(int);

    // cudaMemcpy(a_dev, a, size, cudaMemcpyHostToDevice);

    cudaMemcpy(b_dev, b, size, cudaMemcpyHostToDevice);

    cudaEventRecord(middle, 0);
    cudaEventSynchronize(middle);
    float elapsedMemoryTime;
    cudaEventElapsedTime(&elapsedMemoryTime, start, middle);
    printf("Tempo transferencia memoria Cuda com stream: %3.1f ms \n", elapsedMemoryTime);

    // itera na matrix de resultado
    // para cada item da matrix de resultado, vou usar um thread
    for (long index = 0; index < qtdStreams; index++)
    {
        int deslocamenentoA = (index * colunas);

        // copia uma parte do vetor A
        cudaMemcpyAsync(a_dev + deslocamenentoA, a + deslocamenentoA, sizeLinha, cudaMemcpyHostToDevice, streams[index]);

        dim3 grid(linhas, 1);
        // para subtistir o grid NxN
        // a stream fica com o Linhas e bloco com o colunas
        multiplyMatrixCudaStream<<<grid, thread, sizeLinha * 2, streams[index]>>>(a_dev + deslocamenentoA, b_dev, out_dev, linhas, colunas, colunas, linhas, 1, index);
    }

    cudaMemcpy(resultado, out_dev, sizeResultado, cudaMemcpyDeviceToHost);

    cudaFree(a_dev);
    cudaFree(b_dev);
    cudaFree(out_dev);

    cudaEventRecord(stop, streams[0]);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cuda Stream: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaEventDestroy(middle);
}

void cudaCallMatrixMulDefaultStream(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{

    long linhas = maX;
    long colunas = maY;

    long tamanhoTotal = colunas * linhas;

    long size = sizeof(int) * (tamanhoTotal);
    long sizeResultado = sizeof(int) * (linhas * linhas);

    // criar streams
    cudaStream_t *streams;
    // cria uma stream para cada linha da matriz de resultado
    long qtdStreams = linhas;

    streams = (cudaStream_t *)malloc(sizeof(cudaStream_t) * qtdStreams);

    bool flag = true;
    // alocar uam stream para cada resultado naov ai dar
    // e talvez nao faça muito sentido
    // estou criando um overhead absurdo
    for (long index = 0; index < qtdStreams; index++)
    {
        cudaError_t error = cudaStreamCreateWithFlags(&streams[index], cudaStreamNonBlocking);
        if (error > 0)
        {
            flag = false;
            cout << "Não foi possivel alocar as streams " << cudaGetErrorName(error) << " STREAM ID: " << index << endl;
            return;
        }
    }

    cudaEvent_t start, middle, stop;

    cudaEventCreate(&start);

    cudaEventCreate(&stop);
    cudaEventRecord(start, streams[0]);
    cudaEventCreate(&middle);

    int *a_dev;
    int *b_dev;
    int *out_dev;

    cudaMalloc((void **)&a_dev, size);
    cudaMalloc((void **)&b_dev, size);
    cudaMalloc((void **)&out_dev, sizeResultado);

    int thread = 32;

    // nao precise de blocos
    // dim3 threadsPerBlock(linhas, linhas);

    unsigned long sizeLinha = sizeof(int) * colunas;

    unsigned long sizeResposta = sizeof(int);

    // cudaMemcpy(a_dev, a, size, cudaMemcpyHostToDevice);

    cudaMemcpy(b_dev, b, size, cudaMemcpyHostToDevice);

    cudaEventRecord(middle, 0);
    cudaEventSynchronize(middle);
    float elapsedMemoryTime;
    cudaEventElapsedTime(&elapsedMemoryTime, start, middle);
    //    printf("Tempo transferencia memoria Cuda com stream: %3.1f ms \n", elapsedMemoryTime);

    // itera na matrix de resultado
    // para cada item da matrix de resultado, vou usar um thread
    for (long index = 0; index < qtdStreams; index++)
    {
        int deslocamenentoA = (index * colunas);

        // copia uma parte do vetor A
        cudaMemcpyAsync(a_dev + deslocamenentoA, a + deslocamenentoA, sizeLinha, cudaMemcpyHostToDevice, streams[index]);

        dim3 grid(linhas, 1);
        // para subtistir o grid NxN
        // a stream fica com o Linhas e bloco com o colunas
        gpu_matrix_mult_stream<<<grid, thread, 100, streams[index]>>>(a_dev + deslocamenentoA, b_dev, out_dev, linhas, colunas, linhas, index);
        // cudaStreamSynchronize(streams[index]);
    }

    cudaDeviceSynchronize();
    cudaMemcpy(resultado, out_dev, sizeResultado, cudaMemcpyDeviceToHost);

    cudaFree(a_dev);
    cudaFree(b_dev);
    cudaFree(out_dev);

    cudaEventRecord(stop, streams[0]);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cuda Stream: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaEventDestroy(middle);
}

void cudaCallMatrixMulDefault(int *a, int *b, int *resultado, int maX, int maY, int mbX, int mbY)
{

    cudaEvent_t start, middle, stop;

    cudaEventCreate(&start);
    cudaEventCreate(&middle);

    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);

    int linhas = maX;
    int colunas = maY;

    int tamanhoTotal = colunas * linhas;

    int size = sizeof(int) * (tamanhoTotal);
    int sizeResultado = sizeof(int) * (linhas * linhas);

    int *a_dev;
    int *b_dev;
    int *out_dev;

    cudaMalloc((void **)&a_dev, size);
    cudaMalloc((void **)&b_dev, size);
    cudaMalloc((void **)&out_dev, sizeResultado);

    cudaMemcpy(a_dev, a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(b_dev, b, size, cudaMemcpyHostToDevice);

    int thread = 256;

    dim3 threadsPerBlock(linhas, linhas);

    cudaEventRecord(middle, 0);
    cudaEventSynchronize(middle);

    float elapsedMemoryTime;
    cudaEventElapsedTime(&elapsedMemoryTime, start, middle);
    printf("Tempo transferência memoria Cuda Default: %3.1f ms \n", elapsedMemoryTime);

    gpu_matrix_mult<<<threadsPerBlock, thread>>>(a_dev, b_dev, out_dev, linhas, colunas, linhas);
    cudaDeviceSynchronize();

    cudaMemcpy(resultado, out_dev, sizeResultado, cudaMemcpyDeviceToHost);

    cudaFree(a_dev);
    cudaFree(b_dev);
    cudaFree(out_dev);

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf("Tempo total Cuda Default: %3.1f ms \n", elapsedTime);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaEventDestroy(middle);
}