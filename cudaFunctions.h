#define THREAD_RESPONSABILITY 1024
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

__global__ void multiplyMatrixCuda(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY);

__global__ void multiplyMatrixCudaStream(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY, int streamX, int streamY);

__global__ void multiplyMatrixCudaStream2(int *a, int *b, int *c, int maX, int maY, int mbX, int mbY, int streamX, int streamY);

__global__ void sumMatrixCuda(int *a, int *b, int *c, int tamanho);

__global__ void sumCuda(int *a, int *b, int *c, int tamanho);

__global__ void gpu_matrix_mult(int *a, int *b, int *c, int m, int n, int k);

__global__ void gpu_matrix_mult_stream(int *a, int *b, int *c, int m, int n, int k, int streamIndex);

bool testarSuporteStream();

bool errorCreateStream(int code);
